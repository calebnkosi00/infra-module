# data "aws_ami" "ubuntu" {

#     most_recent = true

#     filter {
#         name   = "name"
#         values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
#     }

#     filter {
#         name = "virtualization-type"
#         values = ["hvm"]
#     }

#     owners = ["099720109477"]
# }
data "aws_subnet_ids" "public" {
    vpc_id   = var.vpc_id
    tags = {
      Type = "Public"
    }
}
resource "aws_instance" "main" {

  ami                  = var.ami
  key_name             = aws_key_pair.ec2_key.key_name
  subnet_id            = var.subnet_id
  instance_type        = var.instance_type
  security_groups      = [aws_security_group.ec2_sg.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  root_block_device {
    volume_type = "gp2"
    volume_size = 20
    delete_on_termination = true
  }
  tags = {
      Name = "${local.env}"
  }
  depends_on = [aws_iam_role_policy_attachment.main]
 
}
resource "aws_eip" "main" {
  instance      = aws_instance.main.id
  vpc           = true
  }

resource "tls_private_key" "main" {
  algorithm = "RSA"
  rsa_bits  = 4096


}
resource "aws_key_pair" "ec2_key" {
    key_name    = "${local.env}" 
    public_key  = tls_private_key.main.public_key_openssh

}

resource "local_file" "pem_file" {
  filename = pathexpand("${path.module}/private.pem")
  file_permission = "400"
  directory_permission = "700"
  sensitive_content = tls_private_key.main.private_key_pem
}

resource "local_file" "public_ip" {
  filename = pathexpand("${path.module}/ip.txt")
  sensitive_content = aws_eip.main.public_ip
}



resource "local_file" "dns_name" {
  filename = pathexpand("${path.module}/dns_name.txt")
  sensitive_content = aws_lb.front_end.dns_name
}

