resource "aws_iam_role" "main" {
  name          = "${local.env}-ec2-instance-role" 
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name          = "${local.env}-ec2-instance-profile"
  role          = aws_iam_role.main.name 
}

resource "aws_iam_policy" "policy" {
  name          = "${local.env}-ec2-instance-policy"
  path          = "/"
  description   = "${local.env}-ec2-instance-policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ssm:UpdateInstanceInformation",
          "ssmmessages:CreateControlChannel",
          "ssmmessages:CreateDataChannel",
          "ssmmessages:OpenControlChannel",
          "ssmmessages:OpenDataChannel"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "s3:GetEncryptionConfiguration"
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })

}

resource "aws_iam_role_policy_attachment" "main" {
  policy_arn        = aws_iam_policy.policy.arn
  role              = aws_iam_role.main.name 
}