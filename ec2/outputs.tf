output "ec2_public_ip" {
  value = aws_eip.main.public_ip
}
output "lb_dns_url" {
    value = aws_lb.front_end.dns_name
}