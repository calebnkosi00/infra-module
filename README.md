# infra-module



## Getting started
To create the aws ec2 infrastructure use terraform, for a region in this case we are using `eu-west-1` you can change it in the `main.tf` file under the deployment directory, see the snippet below.

```
provider "aws" {
  region = "eu-west-1"
}
```

## Prerequisites
- To create the aws ec2 instance you need to have `AWS_ACCESS_KEY_ID`,  `AWS_SECRET_ACCESS_KEY` with the right permissions preferably administrator access.
- You also need your own existing`VPC_ID` and a Public `SUBNET_ID` to cut cost of creating a new VPC
- Terraform for provisioning the infrastructure

## Deployment
- Prepare your local environment before running the terraform scripts

```
$ export AWS_ACCESS_KEY_ID=XXXXXXXXXXXXX
$ export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXX
```
- To deploy the ec2 module under deployment on the `main.tf`
- Change the following `instance_type`, `subnet_id`, `vpc_id`, if you like you can keep `instance_type`, to change the above see the snippet below from the `main.tf` file
```
module "ec2" {
  source        = "../ec2/"
  instance_type = "t2.micro"
  ami           = "ami-08ca3fed11864d6bb"
  subnet_id     = "subnet-xxxxxx"
  vpc_id        = "vpc-xxxxxxx"
}
```
- Now create the ec2 module
```
$ git clone https://gitlab.com/calebnkosi00/infra-module
$ cd infra-module/deployment
$ terraform init
$ terraform plan
$ terraform apply -auto-approve

```
## Post deployment
Things to note after deployment is the private.pem, dns_name.txt and ip.tx files which are located in the ec2 directory.
- The private.pem will be used by the gitlab-ci file for `$PRIVATE_KEY`
- The ip.txt file contains the public ip address which is also going to be used by the gitlab-ci for `$REMOTE_IP` and also to access the application.
- The dns_name.txt file contains the public dns name which can be used to access the application

