provider "aws" {
  region = "eu-west-1"
}
terraform {
  required_version = ">= 1.0"

  required_providers {
    aws = ">= 3.63"
  }
}
module "ec2" {
  source        = "../ec2/"
  instance_type = "t2.micro"
  ami           = "ami-08ca3fed11864d6bb"
  subnet_id     = "subnet-xxxxxx"
  vpc_id        = "vpc-xxxxxxx"
}

